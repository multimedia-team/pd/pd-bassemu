lib.name = bassemu~
class.sources = bassemu~.c 
datafiles = \
	$(wildcard *.pd) \
	$(wildcard *.txt) \
	$(empty)

PDLIBBUILDER_DIR=/usr/share/pd-lib-builder
include $(PDLIBBUILDER_DIR)/Makefile.pdlibbuilder
